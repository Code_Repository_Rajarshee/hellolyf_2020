/**
 * 
 */
package com.hellolyfVC.pages;

import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

import javax.swing.JOptionPane;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

import com.hellollyfVC.utility.WaitElement;


/**
 * @author Rajarshee
 * This Page Contains only Login Page Details
 *
 */

public class LoginPage 
{

	WebDriver driver;
	WaitElement Wait;
	
	@FindBy(xpath="/html/body/app-root/app-login/body/div/div/div[2]"
			+ "/div/div[1]/div/div/label/select")
	WebElement LanguagefromdropDown;
	
	@FindBy(xpath="/html/body/app-root/app-login/body/div/div/div[2]"
			+ "/div/div[2]/div/div[1]/form/ul/li[10]/span/a")
	WebElement privacyPolicy;
	
	//@FindBy(linkText="Forgot your password?")
	//WebElement ForgotPasword;
	
	//@FindBy(id="email_idfp")
	//WebElement ForgotEmail;
	
	@FindBy(id="user_id")
	WebElement Email;
	
	@FindBy(id="myPassword")
	WebElement Password;
	
	@FindBy(xpath="/html/body/app-root/app-login/body/div/div/"
			+ "div[2]/div/div[2]/div/div[1]/form/ul/li[4]/span")
	WebElement Eye;
	
	@FindBy(id="cpatchaTextLogin")
	WebElement captchaValue;
	
	@FindBy(xpath="//button[@class='btn btn-md btn-success login-button']")
	WebElement LoginBtn;
	
	@FindBy(xpath="//*[@id=\"disclaimerModal\"]/div[1]/div/div[2]/div/div/span/label")
	WebElement chkBox;
	
	@FindBy(xpath="//button[@class='btn btn-sm btn-success disclaimer_btn']")
	WebElement Agree;
	
	@FindBy(xpath="//*[@id=\"mobVariModal\"]/div[1]/div/div/div[2]/button[1]")
	WebElement Accept_Yes;
	
	public LoginPage(WebDriver driver)
	{
		this.driver = driver;
	}

	
	public void loginDone(String uname, String pass) throws IOException 
	{
		WaitElement.waitTill(500);
		Select select = new Select(LanguagefromdropDown);
		select.selectByIndex(0);
		WaitElement.waitTill(1000);
		select.selectByIndex(1);
		WaitElement.waitTill(1000);
		select.selectByIndex(2);
		WaitElement.waitTill(1000);
		select.selectByIndex(3);
		WaitElement.waitTill(1000);
		select.selectByIndex(0);
		WaitElement.waitTill(500);
		privacyPolicy.click();
		Set<String> windowhandles = driver.getWindowHandles();
		Iterator<String> iterator = windowhandles.iterator();
		WaitElement.waitTill(500);
		String ParentWindow = iterator.next();
		WaitElement.waitTill(500);
		String ChildWindow = iterator.next();
		WaitElement.waitTill(500);
		driver.switchTo().window(ParentWindow);
		//ForgotPasword.click();
		//WaitElement.waitTill(500);
		//ForgotEmail.sendKeys("rajarshee@ghspl.in");
		WaitElement.waitTill(500);
		Email.sendKeys(uname);
		WaitElement.waitTill(500);
		Password.sendKeys(pass);
		WaitElement.waitTill(500);
		Eye.click();
		WaitElement.waitTill(500);
		Eye.click();
		WaitElement.waitTill(500);
		String captchaVal = JOptionPane.showInputDialog("Please enter the captcha value:");
		captchaValue.sendKeys(captchaVal);
		WaitElement.waitForElement(LoginBtn, 50, driver);
		LoginBtn.click();
		WaitElement.waitTill(5000);
	}
	
	public DoctorDetails AfterclickLoginButton()
	{
		WaitElement.waitTill(500);
		//disclaimer.click();
		Actions action = new Actions(driver);
		action.doubleClick();
		action.moveToElement(chkBox);
		Wait.waitForElement(chkBox, 5000, driver);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", chkBox);
		WaitElement.waitTill(500);
		chkBox.click();
		if(chkBox.isSelected()) {
			System.out.println("Check Box Selected");
		}else {
			System.out.println("Check Box Not Selected");
		}
		WaitElement.waitTill(500);
		Agree.click();
		WaitElement.waitTill(500);
		Accept_Yes.click();
		WaitElement.waitTill(5000);
		return PageFactory.initElements(driver, DoctorDetails.class);
	}
	

	//======================================================================================================================================================================

	/* Verifying valid login */
	public boolean verifyValidLogin()
	{		
		String expURL = "https://www.litmusdx.com/dashboard";
		String actURL = driver.getCurrentUrl();

		if(expURL.equals(actURL)) {
			return true;
		}else {
			return false;
		}

	}

	/* Verifying invalid login */
	public boolean verifyInValidLogin()
	{
		String expURL = "https://www.litmusdx.com/dashboard";
		String actURL = driver.getCurrentUrl();

		if(expURL!=(actURL)) {
			return false;
		}else {
			return true;
		}
	}


}


