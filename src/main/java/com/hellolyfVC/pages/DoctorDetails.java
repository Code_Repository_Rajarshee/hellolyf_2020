package com.hellolyfVC.pages;

import java.awt.AWTException;
import java.io.IOException;

import javax.swing.JOptionPane;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

import com.hellollyfVC.utility.WaitElement;

/**
 * @author Rajarshee
 * This Page Contains only DoctorDetails Page
 *
 */

public class DoctorDetails {

	WebDriver driver;
	WaitElement Wait;
	
	@FindBy(id="experience")
	WebElement experience_Edit;
	
	@FindBy(id="Qualification")
	WebElement qualification_Edit;
	
	@FindBy(xpath="/html/body/app-root/app-user-details/div/div/div/div/form/div/div/\r\n"
			+ "	div/div[4]/div/div[2]/div[1]/div[1]/input")
	WebElement hospitals_Worked;
	
	@FindBy(xpath="//*[@id=\"userSettings\"]/div/div/div/div[4]/div/div[2]/div[1]/div[2]/button")
	WebElement Add_Hospitals_Worked;
	
	@FindBy(xpath="//*[@id=\"userSettings\"]/div/div/div/div[4]/div/div[2]/div[3]/div[2]/button/i")
	WebElement Add_Hospitals_Worked_deleted;
	
	@FindBy(id="awards")
	WebElement awards;
	
	@FindBy(xpath="//*[@id=\"userSettings\"]/div/div/div/div[5]/div/"
			+ "div[2]/div[1]/div[2]/button/i")
	WebElement Add_Awards;
	
	@FindBy(xpath="//*[@id=\"userSettings\"]/div/div/div/div[5]/div/div[2]/div[3]/div[2]/button/i")
	WebElement Awards_deleted;
	
	@FindBy(id="fee")
	WebElement fees;
	
	@FindBy(id="certificate")
	WebElement changePfofileImage;
	
	//@FindBy(xpath="//input[@class='custom-file-input']")
	//WebElement changePfofileImage;
	
	//@FindBy(xpath="//*[@id=\"userSettings\"]/div/div/div/div[7]/div/div[2]/div/label")
	//WebElement changePfofileImage;
	
	@FindBy(id="//*[@id=\"user_sign\"]")
	WebElement uploadSignature;
	
	@FindBy(xpath="//*[@id=\"navbar-mobile\"]/ul/li[4]/div/a[2]")
	WebElement changePassword;
	
	@FindBy(id="password-fieldS")
	WebElement changeNewPassword;
	
	@FindBy(xpath="//*[@id=\"passwordChangeModal\"]/div[1]/div/section/div[2]/div[2]/span")
	WebElement changePasswordEye1;
	
	@FindBy(id="password-fieldC")
	WebElement changeConfirmPassword;
	
	@FindBy(xpath="//*[@id=\"passwordChangeModal\"]/div[1]/div/section/div[3]/div[2]/span")
	WebElement changePasswordEye2;
	
	@FindBy(id="code_cp_text")
	WebElement changeCapthaPassword;
	
	@FindBy(id="valid")
	WebElement changePasswordSubmit;
	
	@FindBy(xpath="//*[@id=\"userSettings\"]/div/div/div/div[9]/div/div[2]/a/input")
	WebElement Submit;
	
	//@FindBy(xpath="/html/body/app-root/app-user-details/div/div/div/div/form/div/"
			//+ "div/div/div[7]/div/div[2]/div/input")
	//WebElement changePfofileImage;
	
	@FindBy(xpath="//*[@id=\"navbar-mobile\"]/ul/li[4]/a")
	WebElement Menu;
	
	@FindBy(xpath="//*[@id=\"navbar-mobile\"]/ul/li[4]/div/a[1]")
	WebElement editPfile;
	
	@FindBy(xpath="//*[@id=\"navbar-mobile\"]/ul/li[4]/div/a[4]")
	WebElement Logout;
	
	@FindBy(xpath="//*[@id=\"docReturnTimeModal\"]/div[1]/div/div[2]/button")
	WebElement availableTime;
	
	
	public DoctorDetails(WebDriver driver)
	{
		this.driver = driver;
	}
	
	public void chkLogout() throws IOException, FindFailed {
		
		WaitElement.waitTill(5000);
		Menu.click();
		WaitElement.waitTill(5000);
		editPfile.click();
		WaitElement.waitTill(5000);
		/*experience_Edit.click();
		WaitElement.waitTill(5000);
		experience_Edit.clear();
		WaitElement.waitTill(5000);
		experience_Edit.sendKeys("25");
		WaitElement.waitTill(5000);
		qualification_Edit.click();
		WaitElement.waitTill(5000);
		qualification_Edit.clear();
		WaitElement.waitTill(5000);
		qualification_Edit.sendKeys("MBBS,MD");
		WaitElement.waitTill(5000);
		hospitals_Worked.click();
		WaitElement.waitTill(5000);
		hospitals_Worked.clear();
		WaitElement.waitTill(5000);
		hospitals_Worked.sendKeys("New City Hospitals");
		WaitElement.waitTill(5000);
		Add_Hospitals_Worked.click();
		WaitElement.waitTill(5000);
		Add_Hospitals_Worked_deleted.click();
		WaitElement.waitTill(5000);
		awards.click();
		WaitElement.waitTill(5000);
		awards.clear();
		WaitElement.waitTill(5000);
		awards.sendKeys("New Awards");
		WaitElement.waitTill(5000);
		Add_Awards.click();
		WaitElement.waitTill(5000);
		Awards_deleted.click();
		WaitElement.waitTill(5000);
		fees.click();
		WaitElement.waitTill(5000);
		fees.clear();
		WaitElement.waitTill(5000);
		fees.sendKeys("500");
		WaitElement.waitTill(5000);
		fees.click();*/
		//Screen screen = new Screen();
		//WaitElement.waitTill(50000);
		//Pattern object1 = new Pattern(".\\Project_Image\\Browse1.PNG");
		//screen.click(object1);
		//WaitElement.waitTill(5000);
		//Actions action = new Actions(driver);
		//action.doubleClick();
		//action.moveToElement(Submit);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", Submit);
		Wait.waitForElement(changePfofileImage, 100000, driver);
		changePfofileImage.click();
		Runtime.getRuntime().exec("D:\\AutoIT\\AutoIT\\FileUploadScript.exe");
		//uploadSignature.click();
		//Submit.click();
		//WaitElement.waitTill(5000);
		//if(changePfofileImage.isSelected()) {
			//System.out.println("Selected");
		//}else {
		//	System.out.println("Not Selected");
		//}
		//WaitElement.waitTill(10000);
		//WaitElement.waitTill(5000);
		//changePfofileImage.sendKeys("C:\\Users\\user\\Desktop\\Image.webp");
		//WaitElement.waitTill(5000);
		//Menu.click();
		//WaitElement.waitTill(5000);
		//changePassword.click();
		//WaitElement.waitTill(5000);
		//changeNewPassword.click();
		//WaitElement.waitTill(5000);
		//changeNewPassword.clear();
		//WaitElement.waitTill(5000);
		//changeNewPassword.sendKeys("Test@123");
		//WaitElement.waitTill(5000);
		//changePasswordEye1.click();
		//WaitElement.waitTill(5000);
		//changeConfirmPassword.click();
		//WaitElement.waitTill(5000);
		//changeConfirmPassword.clear();
		//WaitElement.waitTill(5000);
		//changeConfirmPassword.sendKeys("Test@123");
		//WaitElement.waitTill(5000);
		//changePasswordEye2.click();
		//WaitElement.waitTill(5000);
		//changeCapthaPassword.click();
		//WaitElement.waitTill(5000);
		//String captchaVa2 = JOptionPane.showInputDialog("Please enter the captcha value:");
		//changeCapthaPassword.sendKeys(captchaVa2);
		//WaitElement.waitTill(5000);
		//changePasswordSubmit.click();
		WaitElement.waitTill(5000);
		Logout.click();
		WaitElement.waitTill(5000);
		availableTime.click();
		WaitElement.waitTill(5000);
	}
	
	//===========================================================================================
	
	/* Verifying URL after LogOut */
	public boolean verifyurlafterLogout()
	{		
		String expURL = "https://www.litmusdx.com/login";
		String actURL = driver.getCurrentUrl();

		if(expURL.equals(actURL)) {
			return true;
		}else {
			return false;
		}

	}

}
