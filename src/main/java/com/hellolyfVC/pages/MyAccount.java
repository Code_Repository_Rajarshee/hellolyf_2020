/**
 * 
 */
package com.hellolyfVC.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

import com.hellollyfVC.utility.*;

/**
 * @author Rajarshee
 *
 */
public class MyAccount 
{

	WebDriver driver;
	WaitElement wait;

	@FindBy(xpath="//a[@id='showAppointmentsList']") 
	WebElement shwappointmnt;

	public MyAccount(WebDriver driver)
	{
		this.driver = driver;
	}
	
	
	public void chkclkappintmnt() throws FindFailed {
		WaitElement.waitTill(5000);
		Screen screen = new Screen();
		Pattern object3 = new Pattern(".\\Project_Image\\Appointment_Tab.PNG");
		screen.click(object3);
		WaitElement.waitTill(5000);
	}
	
	public void chkchangePasswrd() throws FindFailed {
		WaitElement.waitTill(5000);
		Screen screen = new Screen();
		Pattern object3 = new Pattern(".\\Project_Image\\Change_Pass_Image.PNG");
		screen.click(object3);
		WaitElement.waitTill(5000);
	}
	
	public void chkPendnConstPg() throws FindFailed {
		WaitElement.waitTill(5000);
		Screen screen = new Screen();
		Pattern object3 = new Pattern(".\\Project_Image\\Pending_Consult_Image.PNG");
		screen.click(object3);
		WaitElement.waitTill(5000);
	}
	
	public void chkSignOutPg() throws FindFailed {
		WaitElement.waitTill(5000);
		Screen screen = new Screen();
		Pattern object3 = new Pattern(".\\Project_Image\\Sign_Out_Image.PNG");
		screen.click(object3);
		WaitElement.waitTill(5000);
	}
	
	/* Verifying Opening Appointment Page or Not */
	public boolean Verifyinappointmntpg()
	{
        String expURL = "https://hellolyf.com/getAppointmentList";
        String actURL = driver.getCurrentUrl();
        if(expURL.equalsIgnoreCase(actURL))
        {
        	return true;
        } else 
        {
        	return false;
        }
		
	}
	
	/* Verifying Opening Change Password Page or Not */
	public boolean VerifychangPasswrdpg()
	{
        String expURL = "https://hellolyf.com/getAppointmentList";
        String actURL = driver.getCurrentUrl();
        if(expURL.equalsIgnoreCase(actURL))
        {
        	return true;
        } else 
        {
        	return false;
        }
		
	}
	
	/* Verifying Opening Pending Consultation Page or Not */
	public boolean VerifyPendgConsltpg()
	{
        String expURL = "https://hellolyf.com/getAppointmentList";
        String actURL = driver.getCurrentUrl();
        if(expURL.equalsIgnoreCase(actURL))
        {
        	return true;
        } else 
        {
        	return false;
        }
		
	}
	
	/* Verifying clicking Sign Out Page or Not */
	public boolean VerifySignOutpg()
	{
        String expURL = "https://hellolyf.com/aftab.jsp";
        String actURL = driver.getCurrentUrl();
        if(expURL.equalsIgnoreCase(actURL))
        {
        	return true;
        } else 
        {
        	return false;
        }
		
	}
}
