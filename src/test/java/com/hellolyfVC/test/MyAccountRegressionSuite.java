/**
 * 
 */
package com.hellolyfVC.test;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.hellolyfVC.pages.DoctorDetails;
import com.hellolyfVC.pages.LoginPage;
import com.hellolyfVC.pages.MyAccount;
import com.hellollyfVC.utility.*;


/**
 * @author Rajarshee
 *
 */
public class MyAccountRegressionSuite 
{

	WebDriver driver;
	//WaitElement wait;
	LoginPage loginPage;
	MyAccount myAcoount;
	DoctorDetails DocDetails;
	//MyAccountRegressionSuite myAccountRegSuite;

	@BeforeMethod
	public void appLaunch()
	{
		driver = BrowserFactory.launchBrowser("chrome", "https://www.hellolyf.com/aftab.jsp");

	}

	@Test(priority = 0 , enabled = false, description="TC_001: Verifying Entering in Apointment section or Not")
	public void chkAppointmnt(){
		try {
			loginPage = PageFactory.initElements(driver, LoginPage.class);
			//loginPage.clickLoginbtn();// Clicking on Login Button
			loginPage.loginDone("9748632179","123456");// Entering valid Password.
			//loginPage.clkloginsecnd();// Again clicking on Login Button.
			//DocDetails.clickaccount();// Click on My Account tab.
			myAcoount.chkclkappintmnt(); // Click on Appointment link
			Assert.assertEquals(false, myAcoount.Verifyinappointmntpg()); // Verifying Enter into Appointment Section or not.
		}catch(Exception e)
		{
			System.out.println(e);
		}
	}

	@Test(priority = 1 , enabled = false, description="TC_001: Verifying Entering in Change Password section or Not")
	public void chkChngPasswrd(){
		try {
			loginPage = PageFactory.initElements(driver, LoginPage.class);
		//	loginPage.clickLoginbtn();// Clicking on Login Button
			loginPage.loginDone("9748632179","123456");// Entering valid Password.
		//	loginPage.clkloginsecnd();// Again clicking on Login Button.
			//DocDetails.clickaccount();// Click on My Account tab.
			myAcoount.chkchangePasswrd(); // Click on Change Password link
			Assert.assertEquals(false, myAcoount.VerifychangPasswrdpg()); // Verifying Enter into Change Password Section or not.
		}catch(Exception e)
		{
			System.out.println(e);
		}
	}
	
	@Test(priority = 2 , enabled = false, description="TC_001: Verifying Entering in Pending Consultation section or Not")
	public void chkPendgConslt(){
		try {
			loginPage = PageFactory.initElements(driver, LoginPage.class);
			//loginPage.clickLoginbtn();// Clicking on Login Button
			loginPage.loginDone("9748632179","123456");// Entering valid Password.
		//	loginPage.clkloginsecnd();// Again clicking on Login Button.
			//DocDetails.clickaccount();// Click on My Account tab.
			myAcoount.chkPendnConstPg(); // Click on Pending Consultation link
			Assert.assertEquals(false, myAcoount.VerifyPendgConsltpg()); // Verifying Enter into Pending Consultation Section or not.
		}catch(Exception e)
		{
			System.out.println(e);
		}
	}
	
	@Test(priority = 3 , enabled = true, description="TC_001: Verifying Clicking Sign Out section or Not")
	public void chkSignOut(){
		try {
			loginPage = PageFactory.initElements(driver, LoginPage.class);
		//	loginPage.clickLoginbtn();// Clicking on Login Button
			loginPage.loginDone("9748632179","123456");// Entering valid Password.
			//loginPage.clkloginsecnd();// Again clicking on Login Button.
		//	DocDetails.clickaccount();// Click on My Account tab.
			myAcoount.chkSignOutPg(); // Click on Sign Out link
			Assert.assertEquals(false, myAcoount.VerifySignOutpg()); // Verifying Enter into Login Page or not.
		}catch(Exception e)
		{
			System.out.println(e);
		}
	}
	
	@AfterMethod
	public void closeBrowser() 
	{
		WaitElement.waitTill(5000);
		//driver.close();
		driver.quit();
	}


}
