/**
 * 
 */
package com.hellolyfVC.test;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.sikuli.script.FindFailed;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.hellolyfVC.pages.LoginPage;
import com.hellollyfVC.utility.*;


/**
 * @author Rajarshee
 *
 */

public class LoginRegressionSuite {

	WebDriver driver;
	//BrowserFactory browserFactory;
	//LoginPage loginPage;
	WaitElement wait;
	LoginPage loginPage;


	
	@BeforeMethod
	public void appLaunch() throws FindFailed, InterruptedException {

		//This will Launch browser and specific URL
		driver = BrowserFactory.launchBrowser("chrome","https://www.litmusdx.com/login");

	}

	@Test(priority = 0 , enabled = true, description="TC_0001: Verifying valid login functionality")
	public void verifyValidLogin() {
		try {
			loginPage = PageFactory.initElements(driver, LoginPage.class);
			//loginPage.loginDone();// Clicking on Login Button
			loginPage.loginDone("aec.abhijit@gmail.com","Test@123");// Entering valid Credencials.
			loginPage.AfterclickLoginButton();// clicking on Login Button.
			//loginPage.clickalertButton(); // Clicking on Alert Button.
			Assert.assertEquals(true, loginPage.verifyValidLogin()); // Verifying valid login scenario.
		}
		catch(Exception e)
		{
			System.out.println(e);
		}

	}

	@Test(priority = 1, enabled = false, description="TC_0002: Verifying invalid login functionality")
	public void verifyInvalidLogin() {
		try 
		{
			loginPage = PageFactory.initElements(driver, LoginPage.class);
			//loginPage.clickLoginbtn(); // Clicking on Login Button
			loginPage.loginDone("97486321790","1234567"); // Entering valid Password.
			//loginPage.clickLoginButton(); // clicking on Login Button.
			//loginPage.clickalertButton(); // Clicking on Alert Button.
			Assert.assertEquals(true, loginPage.verifyInValidLogin()); // Verifying valid login scenario.
		}
		catch(Exception e)
		{
			System.out.println(e);
		}

	}
	
	@AfterMethod
	public void closeBrowser() 
	{
		WaitElement.waitTill(500);
		//driver.close();
		driver.quit();
	}


	

}

