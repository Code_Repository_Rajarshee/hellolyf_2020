/**
 * 
 */
package com.hellolyfVC.test;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.sikuli.script.FindFailed;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.hellollyfVC.utility.BrowserFactory;
import com.hellollyfVC.utility.WaitElement;
import com.hellolyfVC.pages.DoctorDetails;
import com.hellolyfVC.pages.LoginPage;

/**
 * @author Rajarshee
 *
 */
public class DoctorDetailsRegressionSuite {

	WebDriver driver;
	WaitElement wait;
	LoginPage loginPage;
	DoctorDetails doctorDetails;
	
	@BeforeMethod
	public void appLaunch() throws FindFailed, InterruptedException {

		//This will Launch browser and specific URL
		driver = BrowserFactory.launchBrowser("chrome", "https://www.litmusdx.com/login");

	}
	
	@Test(priority = 0 , enabled = true, description="TC_0001: Verifying URL after logout")
	public void verifyURLafterLogout() {
		try {
			loginPage = PageFactory.initElements(driver, LoginPage.class);
			//loginPage.loginDone();// Clicking on Login Button
			loginPage.loginDone("aec.abhijit@gmail.com","Test@123");// Entering valid Credencials.
			doctorDetails = loginPage.AfterclickLoginButton();// clicking on Login Button.
			doctorDetails.chkLogout();
			Assert.assertEquals(true, doctorDetails.verifyurlafterLogout()); // Verifying valid login scenario.
		}
		catch(Exception e)
		{
			System.out.println(e);
		}

	}
	
	
	@AfterMethod
	public void closeBrowser() 
	{
		WaitElement.waitTill(5000);
		//driver.close();
		driver.quit();
	}
}
